# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class NewsappItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class Aljazeera(scrapy.Item):
    title_link = scrapy.Field()
    title_link1 = scrapy.Field()
    title_link2 = scrapy.Field()
    main_title = scrapy.Field()
    small_title_link = scrapy.Field()
    small_title = scrapy.Field()
    sec_head_link_final = scrapy.Field()
    topics_sec_head = scrapy.Field()
    all_links = scrapy.Field()
    final_links = scrapy.Field()

class Aljazeera_details(scrapy.Item):
    title = scrapy.Field()
    short_intro = scrapy.Field()

class Himalayan_times(scrapy.Item):
    main_title = scrapy.Field()
    titles = scrapy.Field()
    title1 = scrapy.Field()
    all_titles = scrapy.Field()

class Himalayan_details(scrapy.Item):
    title = scrapy.Field()
    short_intro = scrapy.Field()

class Khabarhub(scrapy.Item):
    title = scrapy.Field()

class Khabarhub_details(scrapy.Item):
    titles = scrapy.Field()
    short_intro = scrapy.Field()

class Nepalitimes(scrapy.Item):
    title = scrapy.Field()
    title1 = scrapy.Field()
    all_title = scrapy.Field()

class Nepalitimes_details(scrapy.Item):
    title = scrapy.Field()
    short_intro = scrapy.Field()

class Nepalnews(scrapy.Item):
    title = scrapy.Field()
    all_title = scrapy.Field()

class Nepalnews_details(scrapy.Item):
    title = scrapy.Field()
    short_intro = scrapy.Field()