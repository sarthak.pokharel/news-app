from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class RnepalSpider(CrawlSpider):
    newspaper_name = 'The Rising Nepal'
    name = 'risingnepal'

    start_urls = ['https://therisingnepal.org.np/']
    base_urls = ['https://therisingnepal.org.np/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)