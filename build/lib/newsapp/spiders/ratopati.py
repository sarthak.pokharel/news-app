from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class RatopatiSpider(CrawlSpider):
    newspaper_name = 'Ratopati'
    name = 'ratopati'

    start_urls = ['https://english.ratopati.com/']
    base_urls = ['https://english.ratopati.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)