import scrapy
from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class KathSpider(CrawlSpider):
    newspaper_name = 'Kathmandu Post'
    name = 'Kathmandu_post'

    start_urls = ['https://www.kathmandupost.com/']
    base_urls = ['https://www.kathmandupost.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url = url, callback = self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)