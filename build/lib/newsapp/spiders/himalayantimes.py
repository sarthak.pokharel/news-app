from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *
import re

class HimalayanSpider(CrawlSpider):
    newspaper_name = 'The Himalayan Times'
    name = 'himalayan_times'

    start_urls = ['https://thehimalayantimes.com']
    base_urls = ['https://thehimalayantimes.com']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        item = Himalayan_times()
        # print("processing:" + response.url)
        # item['main_title'] = response.xpath('//div[@class = "top_post"]/h2/a/text()').extract()
        item['main_title'] = response.xpath('//div[@class = "top_post"]/h2/a/@href').extract()
        item['titles'] = response.xpath('//h4/a/@href').extract()
        item['title1'] = response.xpath('//h3/a/@href').extract()
        # print(item['main_title'])
        item['all_titles'] = item['main_title'] + item['titles'] + item['title1']
        print(item['all_titles'])
        for urls in item['all_titles']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Himalayan_details()
        p = re.compile(r'(?=\t)((?:.|\n\t+)*)', re.MULTILINE)
        item['title'] = response.xpath('//div[@class = "contentDetail"]/h2/a/text()').extract_first()
        item['short_intro'] = response.xpath('//div[@class = "postDetail mainPost"]/p/text()').extract()[0] + response.xpath('//div[@class = "postDetail mainPost"]/p/text()').extract()[1]
        if response.xpath('//div[@class = "postDetail mainPost"]/p/text()').extract()[1] == '\n\t\t\t\t\t\t\tand\n\t\t\t\t\t\t\t':
            item['short_intro'] = response.xpath('//div[@class = "font-roboto regular line-height-1-5 story-copy color-black-2 f4 mb20"]/p/text()').extract_first()

        elif response.xpath('//div[@class = "postDetail mainPost"]/p/text()').extract()[1] == '\t\t\t\t\t\t\tFollow The Himalayan Times on\n\t\t\t\t\t\t\t':
            item['short_intro'] = response.xpath('//div[@class = "postDetail mainPost"]/p/text()').extract()[0]

        if item['short_intro'] == None:
            item['short_intro'] = response.xpath('//div[@class = "mail-message-header spacer"]/text()').extract_first()

        elif re.findall(p, item['short_intro'][1]) == '\t\t\t\t\t\t\tFollow The Himalayan Times on\n\t\t\t\t\t\t\t':
            item['short_intro'] == item['short_intro'][0]
        yield item