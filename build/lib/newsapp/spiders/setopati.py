from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class SetopatiSpider(CrawlSpider):
    newspaper_name = 'Setopati'
    name = 'setopati'

    start_urls = ['https://en.setopati.com/']
    base_urls = ['https://en.setopati.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)