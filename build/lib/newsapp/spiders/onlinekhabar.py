from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class OkhabarSpider(CrawlSpider):
    newspaper_name = 'Online Khabar'
    name = 'online_khabar'

    start_urls = ['https://english.onlinekhabar.com/']
    base_urls = ['https://english.onlinekhabar.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)