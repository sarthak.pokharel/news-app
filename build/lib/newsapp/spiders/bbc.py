from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class BbcSpider(CrawlSpider):
    newspaper_name = 'BBC news'
    name = 'bbc'

    start_urls = ['https://www.bbc.com/news/topics/cvenzmgyld1t/nepal']
    base_urls = ['https://www.bbc.com/nepali']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)