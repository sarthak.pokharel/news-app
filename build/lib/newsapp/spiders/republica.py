from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class RepublicaSpider(CrawlSpider):
    newspaper_name = 'Republica'
    name = 'republica'

    start_urls = ['https://myrepublica.nagariknetwork.com']
    base_urls = ['https://myrepublica.nagariknetwork.com']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)