from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class SajhapostSpider(CrawlSpider):
    newspaper_name = 'Sajha Post'
    name = 'sajhapost'

    start_urls = ['https://www.sajhapost.com/category/english/']
    base_urls = ['https://www.sajhapost.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)