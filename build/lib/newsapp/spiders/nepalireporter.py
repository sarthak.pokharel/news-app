from scrapy.spiders import CrawlSpider
from scrapy.http import Request

class NreporterSpider(CrawlSpider):
    newspaper_name = 'Nepali Reporter'
    name = 'nepalireporter'

    start_urls = ['https://www.nepalireporter.com/']
    base_urls = ['https://www.nepalireporter.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        print("processing:" + response.url)