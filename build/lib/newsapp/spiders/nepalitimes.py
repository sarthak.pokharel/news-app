from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class NtimesSpider(CrawlSpider):
    newspaper_name = 'Nepali Times'
    name = 'nepalitimes'

    start_urls = ['https://www.nepalitimes.com/']
    base_urls = ['https://www.nepalitimes.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Nepalitimes()
        item['title'] = response.xpath('//h6[@class = "mb-13"]/a/@href').extract()
        item['title1'] = response.xpath('//h6[@class = "mb-0"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1']
        for urls in item['all_title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Nepalitimes_details()
        item['title'] = response.xpath('//h1/text()').extract_first()
        try:
            item['short_intro'] = response.xpath('//span[@class = "dropcaps"]/text()').extract_first() + response.xpath('//div[@class = "elementor-text-editor elementor-clearfix"]/p/text()').extract_first()
        except TypeError:
            item['short_intro'] = []
        yield item