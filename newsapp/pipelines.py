# -*- coding: utf-8 -*-
# import psycopg2
# Define your ite
# m pipelines here
#
from sqlalchemy.orm import sessionmaker
from .models import News, create_job_table, db_connect
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


class NewsappPipeline(object):
    def __init__(self):
        """Create social_urls table"""

        engine = db_connect()
        create_job_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        """Save social links in database"""
        session = self.Session()
        jobs = News(**item)

        try:
            session.add(jobs)
            session.commit()
        except:
            session.rollback()
        finally:
            session.close()

        return item