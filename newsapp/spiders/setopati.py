from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class SetopatiSpider(CrawlSpider):
    newspaper_name = 'Setopati'
    name = 'setopati'

    start_urls = ['https://en.setopati.com/']
    base_urls = ['https://en.setopati.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Setopati()
        item['title'] = response.xpath('//div[@class = "breaking-news-item"]/a/@href').extract()
        item['title1'] = response.xpath('//div[@class = "items media col-md-4"]/a/@href').extract()
        item['title2'] = response.xpath('//div[@class = "items col-md-4"]/a/@href').extract()
        item['title3'] = response.xpath('//div[@class = "items"]/a/@href').extract()
        item['title4'] = response.xpath('//div[@class = "items media"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1'] + item['title2'] + item['title3'] + item['title4']
        for urls in item['all_title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Setopati_details()
        item['title'] = response.xpath('//span[@class = "news-big-title"]/text()').extract_first()
        try:
            item['short_intro'] = response.xpath('//div[@class = "editor-box"]/text()').extract_first().strip()
        except AttributeError:
            item['short_intro'] = response.xpath('//p/text()').extract_first()

        if item['short_intro'] == '':
            item['short_intro'] = response.xpath('//p/text()').extract_first()
        yield item

