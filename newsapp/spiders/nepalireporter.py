from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class NreporterSpider(CrawlSpider):
    newspaper_name = 'Nepali Reporter'
    name = 'nepalireporter'

    start_urls = ['https://www.nepalireporter.com/']
    base_urls = ['https://www.nepalireporter.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Nepalireporter()
        item['title'] = response.xpath('//div[@class = "wrap"]/a/@href').extract()
        item['title1'] = response.xpath('//div[@class = "main-list clearfix"]/a/@href').extract()
        item['title2'] = response.xpath('//div[@class = "card h-100 event-item"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1'] + item['title2']
        for urls in item['all_title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Nepalireporter_details()
        item['title'] = response.xpath('//h1[@class = "single-heading text-center"]/text()').extract_first()
        item['short_intro'] = response.xpath('//p/span/text()').extract_first()
        if item['short_intro'] == None:
            item['short_intro'] = response.xpath('//p/text()').extract_first()
        yield item