from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class OkhabarSpider(CrawlSpider):
    newspaper_name = 'Online Khabar'
    name = 'online_khabar'

    start_urls = ['https://english.onlinekhabar.com/']
    base_urls = ['https://english.onlinekhabar.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Onlinekhabar()
        item['title'] = response.xpath('//h3/a/@href').extract()
        for urls in item['title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Onlinekhabar_details()
        item['title'] = response.xpath('//h1[@class = "news-head"]/text()').extract_first()
        item['short_intro'] = response.xpath('//p/text()').extract()[0] + response.xpath('//p/text()').extract()[1]
        if item['title'] == None:
            item['title'] = response.xpath('//h1/span/text()').extract_first()

        yield item