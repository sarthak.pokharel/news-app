from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class KhabarhubSpider(CrawlSpider):
    newspaper_name = 'Khabar Hub'
    name = 'khabarhub'

    start_urls = ['https://english.khabarhub.com/']
    base_urls = ['https://english.khabarhub.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Khabarhub()
        item['title'] = response.xpath('//div[@class = "customScroll newsUpdate"]/ul/li/a/@href').extract()
        for urls in item['title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Khabarhub_details()
        item['title'] = response.xpath('//h2[@class = "singleTitle"]/text()').extract_first().split('\n')[1]
        item['short_intro'] = response.xpath('//div[@class = "singleDetail"]/p/text()').extract_first()
        yield item