from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class SajhapostSpider(CrawlSpider):
    newspaper_name = 'Sajha Post'
    name = 'sajhapost'

    start_urls = ['https://www.sajhapost.com/category/english/']
    base_urls = ['https://www.sajhapost.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Sajhapost()
        item['title'] = response.xpath('//strong[@class = "title-common-small"]/a/@href').extract()
        for urls in item['title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Sajhapost_details()
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['short_intro'] = response.xpath('//div[@class = "newsPost-detailText-content"]/p/text()').extract_first()
        yield item