from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class RnepalSpider(CrawlSpider):
    newspaper_name = 'The Rising Nepal'
    name = 'risingnepal'

    start_urls = ['https://risingnepaldaily.com/']
    base_urls = ['https://risingnepaldaily.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Risingnepal()
        item['title'] = response.xpath('//h2[@class = "post-title"]/a/@href').extract()
        item['title1'] = response.xpath('//h5/a/@href').extract()
        item['title2'] = response.xpath('//div[@class = "trendings"]/a/@href').extract()
        item['title3'] = response.xpath('//div[@class = "trending-right"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1'] + item['title2'] + item['title3']
        for urls in item['all_title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Risingnepal_details()
        item['title'] = response.xpath('//div[@class = "news-left"]/h3/text()').extract_first()
        item['short_intro'] = response.xpath('//div[@class = "newstext"]/p/text()').extract()[0]
        if item['short_intro'] == '\xa0':
            item['short_intro'] = response.xpath('//div[@class = "newstext"]/p/text()').extract()[1]
        yield item