import scrapy
from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class KathSpider(CrawlSpider):
    newspaper_name = 'Kathmandu Post'
    name = 'Kathmandu_post'

    start_urls = ['https://kathmandupost.com/']
    base_urls = ['https://kathmandupost.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url = url, callback = self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Kathmandu_post()
        item['title'] = response.xpath('//h2/a/@href').extract()
        item['title1'] = response.xpath('//h3/a/@href').extract()
        item['title2'] = response.xpath('//h5/a/@href').extract()
        item['title3'] = response.xpath('//h1/a/@href').extract()
        item['title4'] = response.xpath('//article[@class = "article-image"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1'] + item['title2'] + item['title3'] + item['title4']

        for urls in item['all_title']:
            url = 'https://kathmandupost.com' + urls
            yield Request(url, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Kathmandupost_details()
        item['title'] = response.xpath('//h1/text()').extract_first()
        item['short_intro'] = response.xpath('//section[@class = "story-section"]/p/text()').extract_first()
        if len(item['short_intro'].split("PM")) == 2:
            item['short_intro'] = response.xpath('//section[@class = "story-section"]/p/text()').extract()[1]
        yield item
