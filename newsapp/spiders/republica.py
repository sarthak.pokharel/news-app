from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class RepublicaSpider(CrawlSpider):
    newspaper_name = 'Republica'
    name = 'republica'

    start_urls = ['https://myrepublica.nagariknetwork.com']
    base_urls = ['https://myrepublica.nagariknetwork.com']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Nagariknews()
        item['title'] = response.xpath('//div[@class = "banner top-breaking"]/a/@href').extract()
        item['title1'] = response.xpath('//div[@class = "news-topics"]/a/@href').extract()
        item['title2'] = response.xpath('//div[@class = "list-group"]/ul[@class = "list-inline news-list"]/li/a/@href').extract()
        item['title3'] = response.xpath('//div[@class = "caption"]/a/@href').extract()
        item['all_title'] = item['title'] + item['title1'] + item['title2'] + item['title3']
        for urls in item['all_title']:
            urls = 'https://myrepublica.nagariknetwork.com' + urls
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Nagarik_details()
        try:
            item['title'] = response.xpath('//div[@class = "main-heading"]/h2/text()').extract_first().strip()
        except AttributeError:
            item['title'] = response.xpath('//h2[@class = "article__header__title"]/text()').extract_first()
        item['short_intro'] = response.xpath('//div[@id = "newsContent"]/p/text()').extract_first()
        yield item