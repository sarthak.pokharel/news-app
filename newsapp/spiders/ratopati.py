from scrapy.spiders import CrawlSpider
from scrapy.http import Request

from newsapp.items import *

class RatopatiSpider(CrawlSpider):
    newspaper_name = 'Ratopati'
    name = 'ratopati'

    start_urls = ['http://english.ratopati.com/']
    base_urls = ['http://english.ratopati.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Ratopati()
        item['title'] = response.xpath('//div[@class = "item-content"]/h2/a/@href').extract()
        item['title1'] = response.xpath('//div[@class = "item-content"]/h3/a/@href').extract()
        item['all_title'] = item['title'] + item['title1']
        for urls in item['all_title']:
            url = 'http://english.ratopati.com' + urls
            yield Request(url, callback = self.crawl_inside)


    def crawl_inside(self, response):
        item = Ratopati_details()
        item['title'] = response.xpath('//div[@class = "article-head"]/h1/text()').extract_first()
        item['short_intro'] = response.xpath('//p/text()').extract_first()
        if item['short_intro'] == ' Om Sharma':
            item['short_intro'] = response.xpath('//h4[@id = "headline"]/text()').extract_first()

        if item['short_intro'] == None:
            item['short_intro'] = response.xpath('//h3/text()').extract_first()

        if item['short_intro'] == '':
            item['short_intro'] = response.xpath('//p/text()').extract()[3]

        yield item