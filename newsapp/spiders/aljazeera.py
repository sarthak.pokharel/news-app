from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *

class JazeeraSpider(CrawlSpider):
    newspaper_name = 'Al Jazeera'
    name = 'aljazeera'

    start_urls = ['https://www.aljazeera.com/topics/country/nepal.html']
    base_urls = ['https://www.aljazeera.com/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        item= Aljazeera()
        print("processing:" + response.url)
        item['title_link'] = ['https://www.aljazeera.com' + x for x in response.xpath('//div[@class = "top-feature-overlay-cont"]/a/@href').extract()]
        item['main_title'] = response.xpath('//h2[@class = "top-sec-title"]/text()').extract()
        item['title_link1'] = ['https://www.aljazeera.com' + x for x in response.xpath('//div[@class = "topFeature-sblock-wr"]/a/@href').extract()]
        item['small_title'] = response.xpath('//h2[@class = "top-sec-smalltitle"]/text()').extract()
        sec_head_link = ['https://www.aljazeera.com' + x for x in response.xpath('//div[@class = "col-sm-7 topics-sec-item-cont"]/a/@href').extract()]
        item['title_link2'] = [s.replace('https://www.aljazeera.comhttps://interactive.aljazeera.com', 'https://interactive.aljazeera.com') for s in sec_head_link]
        item['topics_sec_head'] = response.xpath('//h2[@class = "topics-sec-item-head"]/text()').extract()
        item['all_links'] = item['title_link'] + item['title_link1'] + item['title_link2']
        item['final_links'] = list(filter(lambda x: '/news/' in x, item['all_links']))
        for url in item['final_links']:

            yield Request(url, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Aljazeera_details()
        item['title'] = response.xpath('//div[@class = "article-heading"]/h1[@class = "post-title"]/text()').extract_first()
        item['short_intro'] = response.xpath('//p[@class = "p1"]/text()').extract_first()
        if item['short_intro'] == None:
            item['short_intro'] = response.xpath('//p[@class = "speakable"]/text()').extract_first()
            if item['short_intro'] == None:
                item['short_intro'] = response.xpath('//p/text()').extract_first()
        elif item['short_intro'] == []:
            item['short_intro'] = response.xpath('//p[@class = "speakable"]/text()').extract_first()
        yield item