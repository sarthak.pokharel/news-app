from scrapy.spiders import CrawlSpider
from scrapy.http import Request
from newsapp.items import *
import re

class NepalnewsSpider(CrawlSpider):
    newspaper_name = 'Nepal News'
    name = 'nepal_news'


    start_urls = ['https://www.nepalnews.net/']
    base_urls = ['https://www.nepalnews.net/']

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url=url, callback=self.news_details)

    def news_details(self, response):
        # print("processing:" + response.url)
        item = Nepalnews()
        item['title'] = response.xpath('//div[@class = "single_head"]/h6/a/@href').extract()
        item['all_title'] = []
        for titles in item['title']:
            title = titles.split("https://") or titles.split("http://")
            if len(title) == 2:
                pass
            if len(title) == 1:
                item['all_title'].append('https://www.nepalnews.net' + title[0])
        for urls in item['all_title']:
            yield Request(urls, callback = self.crawl_inside)

    def crawl_inside(self, response):
        item = Nepalnews_details()
        item['title'] = response.xpath('//div[@class = "title_text"]/h2/a/text()').extract_first()
        item['short_intro'] = response.xpath('//p/text()').extract()[2]
        yield item
