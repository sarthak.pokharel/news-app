from sqlalchemy import create_engine, Column, Integer, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

from .database import DATABASE


DeclarativeBase = declarative_base()


def db_connect():
    """Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance.
    """
    return create_engine(URL(**DATABASE))


def create_job_table(engine):
    DeclarativeBase.metadata.create_all(engine)

class News(DeclarativeBase):
    """sqlalchemy model"""
    __tablename__ = "news_scraped"

    id = Column(Integer, primary_key=True)
    title = Column('title', String)
    short_intro = Column('short_intro', Date)

    def __init__(self,
                 title, short_intro):
        self.title = title
        self.short_intro = short_intro